Name:           rpkg
Version:        1.65
Release:        1

Summary:        Python library for interacting with rpm+git
License:        GPLv2+ and LGPLv2
URL:            https://pagure.io/rpkg
BuildArch:      noarch
Source0:        https://pagure.io/releases/rpkg/%{name}-%{version}.tar.gz
Source1:        rpkg.rpmlintrc

# This is intended for Python 3 only, hence also no Python version in the name.
%global __pytest /usr/bin/pytest%(test %{python3_pkgversion} == 3 || echo -%{python3_version})
%global pytest %{expand:\\\
  CFLAGS="${CFLAGS:-${RPM_OPT_FLAGS}}" LDFLAGS="${LDFLAGS:-${RPM_LD_FLAGS}}"\\\
  PATH="%{buildroot}%{_bindir}:$PATH"\\\
  PYTHONPATH="${PYTHONPATH:-%{buildroot}%{python3_sitearch}:%{buildroot}%{python3_sitelib}}"\\\
  PYTHONDONTWRITEBYTECODE=1\\\
  %{?__pytest_addopts:PYTEST_ADDOPTS="${PYTEST_ADDOPTS:-} %{__pytest_addopts}"}\\\
  %__pytest}

# Fix for bug 1579367
# Due to https://pagure.io/koji/issue/912, python[23]-koji package does not
# have egginfo.
# rpm-py-installer is required as a proxy to install RPM python binding
# library, so rpm is the actual requirement that must be present in the
# requires.txt. But, rpkg has to work in all active Fedora and EPEL releases,
# and there is only old rpm-python package in EL6 and 7, so just simply to
# remove rpm-py-installer for now.
Patch0:         remove-koji-and-rpm-py-installer-from-requires.patch
Patch1:         0001-Do-not-use-pytest-related-dependencies-temporarily.patch
Patch2:         0002-Remove-pytest-coverage-execution.patch
Patch4:         0004-fedpkg-local-does-not-show-rpmbuild-output.patch
Patch5:         0005-Fixes-for-exploded-SRPM-layouts.patch
Patch6:         0006-mockbuild-escape-rpm-command-under-mock.patch

%description
Python library for interacting with rpm+git

%package -n python3-%{name}
Summary:        %{summary}

BuildRequires:  python3-devel
BuildRequires:  python3-GitPython
BuildRequires:  python3-koji >= 1.24
BuildRequires:  python3-libmodulemd
BuildRequires:  python3-argcomplete
BuildRequires:  python3-CCColUtils
BuildRequires:  python3-openidc-client
BuildRequires:  python3-pycurl
BuildRequires:  python3-six >= 1.9.0
BuildRequires:  python3-requests
BuildRequires:  python3-setuptools
BuildRequires:  python3-pytest
BuildRequires:  python3-PyYAML
BuildRequires:  rpmdevtools
BuildRequires:  git-core

Requires:       mock
Requires:       openEuler-rpm-config
Requires:       rpm-build
Requires:       rpmlint
Requires:       rpmdevtools

Requires:       python3-argcomplete
Requires:       python3-GitPython >= 0.2.0
Requires:       python3-CCColUtils
Requires:       python3-koji >= 1.24
Requires:       python3-libmodulemd
Requires:       python3-rpmautospec
Requires:       python3-rpm
Requires:       python3-pycurl
Requires:       python3-six >= 1.9.0
Requires:       python3-PyYAML

Requires:       %{name}-common = %{version}-%{release}


%description -n python3-%{name}
A python library for managing RPM package sources in a git repository.

%package common
Summary:        Common files for %{name}

# Files were moved from python2-rpkg in that version
Conflicts:      python2-rpkg < 1.52-2
Conflicts:      pyrpkg < 1.52-2

%description common
Common files for python2-%{name} and python3-%{name}.


%prep
%autosetup -p1

# Removes section from setup.py that is relevant only for pip and
# is not compatible with in RHEL-6 tools
sed -i -n '/extras_require/,/}/!p' setup.py

%build
%py3_build


%install
%py3_install

# Create configuration directory to holding downstream clients config files
# that are built on top of rpkg
%{__install} -d $RPM_BUILD_ROOT%{_sysconfdir}/rpkg

example_cli_dir=$RPM_BUILD_ROOT%{_datadir}/%{name}/examples/cli
%{__install} -d $example_cli_dir

# Install example CLI to rpkg own data directory
%{__install} -d ${example_cli_dir}%{_bindir}
%{__install} -d ${example_cli_dir}%{_sysconfdir}/bash_completion.d
%{__install} -d ${example_cli_dir}%{_sysconfdir}/rpkg

%{__install} -p -m 0644 bin/rpkg ${example_cli_dir}%{_bindir}
%{__install} -p -m 0644 etc/bash_completion.d/rpkg.bash ${example_cli_dir}%{_sysconfdir}/bash_completion.d
%{__install} -p -m 0644 etc/rpkg/rpkg.conf ${example_cli_dir}%{_sysconfdir}/rpkg

# need to run check as non-root
%check
%pytest

%files -n python3-%{name}
%doc README.rst CHANGELOG.rst
%license COPYING COPYING-koji LGPL
%{python3_sitelib}/pyrpkg
%{python3_sitelib}/%{name}-%{version}-py*.egg-info

%files common
%{_datadir}/%{name}
%{_sysconfdir}/rpkg

%changelog
* Fri Mar 24 2023 lichaoran <pkwarcraft@hotmail.com> - 1.65-1
- Initial package
